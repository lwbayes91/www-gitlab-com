---
layout: handbook-page-toc
title: "Definitive Guide to Self-Managed Analysis"
description: "This page defines Self-Managed and provides directions for Self-Managed Data Analysis"
---

## On this page
- TOC
{:toc}

{::options parse_block_html="true" /}

---
## Self-Managed 

**SELF-MANAGED** is an edition of GitLab that customers manage within their own environment and not within GitLab SaaS. [Choosing between GitLab SaaS and self-managed subscriptions](https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/#why-you-probably-want-gitlab-saas) provides a great overview of these two editions.
 
### Self-Managed Data Components

* [Service Ping](https://docs.gitlab.com/ee/development/service_ping/)
* Snowplow

## Self-Managd Analysis

`Coming Soon`

### Self-Managed Instances

`Coming Soon`

### Self-Managed Product Usage

`Coming Soon`

### Self-Managed Growth

`Coming Soon`

### Self-Managed Geolocation

`Coming Soon`
