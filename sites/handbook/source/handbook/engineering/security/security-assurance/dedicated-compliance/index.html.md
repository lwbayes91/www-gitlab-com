---
layout: handbook-page-toc
title: "Security Compliance, Dedicated Markets Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i class="fas fa-bullseye" style="color:rgb(110,73,203)" aria-hidden="true"></i> Security Compliance, Dedicated Markets Mission

Our Mission is to advance customer trust with a focus on customers operating in highly regulated industries or who otherwise have unique security and compliance requirements. We will accomplish this mission by:

1. Enabling [GitLab Dedicated](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#gitlab-dedicated) to be the most trusted DevSecOps offering in the market, demonstrated by security certifications and attestations.
1. Applying compliance automation and guardrails to minimize friction and enable development and infrastructure teams.
1. Use our own product ([dogfooding](https://about.gitlab.com/handbook/values/#dogfooding)) to meet key security controls, improve our offering, and demonstrate to customers how they can do the same.

For more information on the direction of the GitLab Dedicated category, please see [this page](https://about.gitlab.com/direction/saas-platforms/dedicated/).

## <i class="far fa-lightbulb" style="color:rgb(110,73,203)" aria-hidden="true"></i> Core Competencies
As a member of the [Security Assurance](/handbook/engineering/security/security-assurance/) sub-department, and fork of the existing [Security Compliance team](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/), we share many of the same core competencies. The difference between our teams is in the product/system scope (GitLab Dedicated and any future offerings for highly regulated markets) and the security certifications we are pursuing.

1. [Internal Security Compliance Audit](/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html)
   * [GCF Continuous Control Monitoring](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
   * Continuous Control Monitoring Automation
   * [User Access Reviews](/handbook/engineering/security/security-assurance/security-compliance/access-reviews.html)
   * [Business Continuity Plan (BCP)](https://about.gitlab.com/handbook/business-technology/gitlab-business-continuity-plan/) and [Information System Continuity (ISCP)](https://about.gitlab.com/handbook/engineering/security/Information-System-Contingency-Plan-ISCP.html) testing
1. [Security Certifications](/handbook/engineering/security/security-assurance/security-compliance/certifications.html)
   * External Audits
   * [Gap Assessments/Readiness Assessments](/handbook/engineering/security/security-assurance/security-compliance/gap-analysis-program.html)
1. [Observation and Remediation](/handbook/engineering/security/security-assurance/security-compliance/observation-management-procedure.html)
   * Security Control Testing Activities (CCM)
   * Customer Assurance Activities
   * External Audits
   * Ad-hoc Observations
   * Gap Assessment Activities

Some of our work is [not public](https://about.gitlab.com/handbook/communication/confidentiality-levels/#not-public) for now. Please see the [internal handbook](https://internal-handbook.gitlab.io/handbook/engineering/) to find out more about what our team is working on, or reach out to us.

## <i id="biz-tech-icons" class="fas fa-tasks"></i>Metrics and Measures of Success

1. [Security Control Health](/handbook/engineering/security/performance-indicators/#security-control-health)
1. [Security Observations](/handbook/engineering/security/performance-indicators/#security-observations-tier-3-risks)

## <i class="fas fa-id-card" style="color:rgb(110,73,203)" aria-hidden="true"></i> Contact the Team

* Slack
   * Feel free to tag is with `@dedicated_compliance`
   * The #sec-assurance slack channel is the best place for questions relating to our team (please add the above tag)
* Tag us in GitLab
   * `@gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/team-security-dedicated-compliance`
* Email
   * `security-compliance@gitlab.com`
* Here are our team's GitLab.com [subgroups and projects](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/team-security-dedicated-compliance)

* Interested in joining our team? Check out more [here](https://about.gitlab.com/job-families/engineering/security-compliance/#dedicated-markets)!
