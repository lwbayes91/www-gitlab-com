---
layout: handbook-page-toc
title: "Public Sector Go To Market"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Public Sector Message House

#### Booth Messaging / Tagline

* **Secure the Speed to Mission.**
*Deliver more secure code faster and better.*

<br>

#### Positioning Statement

For public sector government buyers whose existing DevOps solutions prevent their secure speed to mission to protect the nation because they rely on manual processes, dated policies, and antiquated systems that are complex, expensive to maintain and are susceptible to vulnerabilities.

GitLab is the one DevOps Platform that empowers organizations to deliver secure software, demonstrate compliance, and automate the end-to-end DevOps experience in a single application.

#### Short Description
GitLab empowers government agencies to deliver secure software so they are  successful with their digital transformation efforts to protect and secure the nation - securing the speed to mission.


#### Long Description
Government agencies strive to be able to serve the public by providing optimal experiences to their communities. One of the best ways to do this is to ensure a successful DevOps digital transformation overcoming manual processes, dated policies, and IT infrastructures that can jeopardize the experiences providing positive civilian experiences.

GitLab is the one DevOps platform for the public sector. It enables government software buyers to deliver secure software faster, strengthen compliance, and automate the end-to-end DevOps experience empowering organizations to serve the public - securing the speed to mission.


#### User Personas
* [Allison](https://about.gitlab.com/handbook/product/personas/#allison-application-ops) (Application Ops)
* Program Managers
* Contracting Officer

#### Buyer Personas
* [Skyler](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#skyler---the-chief-information-security-officer) (Chief Information Security Officer)
* Chief Technology Officer (CTO)
* Chief Information Officer (CIO)
* [Dakota](https://about.gitlab.com/handbook/product/personas/#dakota-application-development-director) (Application Development Director)

#### Key-Value 1: Embedded Security
**Promise:** Initiate a proactive security strategy where vulnerabilities are discovered earlier (shifting left) in the Software Delivery Lifecycle (SDLC) with security and compliance embedded within the end to end devSecOps workflow enabling you to understand and manage risk.

**Pain Point:** Increasing cyber-attacks and cybersecurity threats from internal and external entities. 

 **Why GitLab:** 
 * Continuously address application vulnerabilities before code ships. 
 * Support for [Zero Trust](https://about.gitlab.com/handbook/security/#:~:text=In%20our%20case%2C%20Zero%20Trust,to%20authenticate%20and%20be%20authorized.) architecture with authentication and authorization of devices, end-users, endpoints, or assets, users and approvers. 
 * Simplified [workflows](https://docs.gitlab.com/ee/topics/gitlab_flow.html) that comply with internal processes, controls, and federal regulations ensuring the software being developed is secured per the National Institute of Standards and Technology's (NIST) [Secure Software Development Framework](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-218.pdf) (SSDF). 
* Integrated security training
* [Rule mode for scan result policies](https://release-14-9.about.gitlab-review.app/releases/2022/03/22/gitlab-14-9-released/#rule-mode-for-scan-result-policies)
* [Scheduled DAST Scans](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released/#scheduled-dast-scans)
* [Infrastructure as Code security scanning](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/#introducing-infrastructure-as-code-iac-security-scanning)


#### Key-Value 2: Automation for software factory deployment
**Promise:** Deploy an easy to manage software factory  quickly to build, test, and deliver applications.

**Pain Point:** Manual processes, dated policies, and aging technology preventing digital transformation.

**Why GitLab:**
* Software factory that accelerates the delivery of critical capabilities with flexible, automatic business, development, operations, and security workflows in one platform speed up Authority to Operate (ATO).
* [Agile project management, distributed version control](https://about.gitlab.com/solutions/agile-delivery/) based on Git, and automated [CI/CD](https://docs.gitlab.com/ee/ci/).

#### Key-Value 3: Modernize IT infrastructure
**Promise:** Protect antiquated IT infrastructure from compromised applications and streamline control points for end-to-end software.

**Pain Point:** Aging technology leaves agencies more vulnerable to cyber attacks, obstructs budgeting for long term IT improvements, and lead to overspending resulting in in unnecessary IT support.

**Why GitLab**
* One DevOps platform empowers organizations to maximize the overall return on software development simplifying their [digital transformation](https://about.gitlab.com/images/press/gitlab-data-sheet.pdf).
* Support for [self-hosting](https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/) options and low to high workflows aligning to the [Improving the Nation’s Cybersecurity](https://www.whitehouse.gov/briefing-room/statements-releases/2021/05/12/fact-sheet-president-signs-executive-order-charting-new-course-to-improve-the-nations-cybersecurity-and-protect-federal-government-networks/) Executive Order. 

#### Key-Value 4: Visibility into the software supply chain
**Promise:** Software Bill of Materials (SBOM) securing the software development environment and identification of software dependencies providing deep application visibility.

**Pain Point:** Weak software supply chain that leads to security breaches and invisible security threats.

**Why GitLab**
* [SBOM](https://about.gitlab.com/solutions/supply-chain/) for an end-to-end secure platform to help protect multiple attacks, including protection for internal code, external sources, and even the build process.
* Aligns with the [“Improving the Nation’s Cybersecurity”](https://www.whitehouse.gov/briefing-room/statements-releases/2021/05/12/fact-sheet-president-signs-executive-order-charting-new-course-to-improve-the-nations-cybersecurity-and-protect-federal-government-networks/) Executive Order.

#### Key-Value 5: Accelerate ATO with automated Compliance
**Promise:** Automate compliance management to significantly reduce time to achieve authority to operate (ATO).

**Pain Point:** Agencies are burdened  by the ATO process at the end of the SDLC adding unplanned and unscheduled work to delivery timelines.

**Why GitLab**
* [Complaint workflow automation](https://about.gitlab.com/solutions/compliance/) enforces policies and separation of duties while reducing risk.
* One place to view compliance data. 
* GitLab groups and projects ensure they are operating appropriately.
* Easily identify when a project is subject to compliance requirements. 
* Observability
* Enhanced Audit Events: [Streaming Audit Events](https://about.gitlab.com/releases/2022/01/22/gitlab-14-7-released/#streaming-audit-events), [MR Approval Auditing](https://gitlab.com/groups/gitlab-org/-/epics/7268), [Compliance framework changes](https://about.gitlab.com/releases/2021/07/22/gitlab-14-1-released/#audit-events-for-project-compliance-frameworks-changes)
* [Security Approval Policies](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#security-approval-policies)

#### Proof Points
* [How the U.S. Army Cyber School created “Courseware as Code” with GitLab](https://about.gitlab.com/customers/us_army_cyber_school/)

* [How Chicago’s Cook County assesses economic data with transparency and version control](https://about.gitlab.com/customers/cook-county/)

* [The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects](https://about.gitlab.com/customers/uw/)

<br>

### SWOTT Analysis
**Strenghts**
* Empowers users to be collaborative which brings efficiency 
*Multiple functionalities align to NIST guidelines from Executive Order
* Shift Left - vulnerabilities are discovered earlier in the SDLC with several security scanning processes - DAST, SAST, fuzz testing, secret detection, and more 
*Forrester recognition as a Leader for the Forrester CI Tools Wave
* Hardened Container story/DoD software factory relationships
*Low-to-high side DevOps (Cross-Domain DevOps)
* Single application for entire DevOps lifecycle
* Leader in CI & Cloud Native CI/Leading SCM/Optimized for k8s
* Built-in continuous and automated security & compliance
* Flexible hosting options/Software can be deployed anywhere
* End-to-end insight and visibility
* Open source 
* Enables rapid innovation
* Provides collaborative and transparent CX

**Weaknesses**
* Not FedRamp authorized until FY2024
* Use cases where third party integration is needed to work with Kubernetes
* More awareness is needed for the EO/NIST story
* Lacking messaging and marketing assets for the different verticals within the Public Sector, i.e. SLED, Civilian, Intelligence, DOD

**Opportunities**
* Federal government increased the 2022 budget by 11% to $10.9 billion for civilian cybersecurity actvities
* Authorizations to grow in the sector:
  * Will be FIPS 140-2 and RAR authorized in FY2023
  * Will be FedRamp authorized FY2024
* Continue to tell the story how GitLab aligns to NIST/EO - drumbeat the market
* Built-in container registry and Kubernetes integration for easy containerization and cloud native development
* Enablement of Digital Transformation 
* Continuing cloud modernization/Cloud Native and 100% cloud agnostic

**Threats**
* Strong Microsoft environments tend to get free tools (GitHub) when they purchase the Cloud + Office
* Strong preference is given to approved, incumbent solutions, i.e. Fortify and SonarQube
* Microsoft/Google announced products/solution for NIST/EO
* Lack of GitLab ability to maneuver fast enough to respond/competitors with FedRAMP authorization and EO/NIST alignmnent

<br>

### Content 
Ungated asset links can be found on [this document](https://docs.google.com/document/d/1Yvqq-X2fcUgwSKC-UtIP7qLnywIs9BA65F5NgyR4PoQ/edit?usp=sharing). 

* [Biden administration accelerates software supply chain security expectations a year into Executive Order (blog)](https://about.gitlab.com/blog/2022/05/12/biden-administration-celebrates-1-year-anniversary-of-eo-by-accelerating-software-supply-chain-security/)
* [GitLab and AWS Make DevOps Easier](https://docs.google.com/document/d/1ilcgjvHBJp0zK5vGQrnvuTUoHDZFXNHbGV6A6iqTACk/edit?usp=sharing)
* [Comply with NIST's secure software supply chain framework with GitLab (blog)](https://about.gitlab.com/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/)  
* [GitLab Public Sector Capabilities Statement](https://about.gitlab.com/images/press/gitlab-capabilities-statement.pdf)  
* [Speed to Mission (whitepaper)](https://about.gitlab.com/resources/whitepaper-speed-to-mission/)
* [The Hidden Costs of DevOps Toolchains (with guest speaker from Forrester)(webcast)](https://about.gitlab.com/webcast/simplify-to-accelerate/)
* [DevOps Powering Your Speed to Mission (webcast)](https://about.gitlab.com/webcast/devops-speed-to-mission/)
* [Mission Mobility: A DevSecOps Discussion (webcast)](https://about.gitlab.com/webcast/mission-mobility/)
* [Government Matters Technology Leaders Innovation Series: Low-to-High Collaboration (with Marc Kriz) (video)](https://youtu.be/RvZSTCbPkiE)
* [Modernizing Government IT through DevOps (whitepaper)](https://about.gitlab.com/resources/whitepaper-modernizing-government-it/)
* [Cross Domain DevSecOps: Low-to-High Side Collaboration (whitepaper)](https://page.gitlab.com/resources-whitepaper-cross-domain-devsecops.html) 
* [DevSecOps: How Proactive Security Integration Reduces Your Agency's Risks & Vulnerability(article)](https://drive.google.com/file/d/1G5UHVBMIeoYZHRjPt1owk2b_aQShDffJ/view?usp=sharing)
* [The Right Application Platform Can Help DoD Develop Its DevSecOps Culture (article)](https://page.gitlab.com/resources-article-RightPlatformDoD.html)
* [GitLab and the USDS Playbook: Delivering on the Promise of Digital Services (ebook)](https://page.gitlab.com/resources-ebook-gitlab-usds-playbook.html)
* [Software as the New Operational Excellence (webcast)](https://page.gitlab.com/resources-webcast-software-delivery-new-excellence.html)
* [GitLab’s Hardened Container Image for Secure Software Development](https://drive.google.com/file/d/1o2pSl7yUKaHwCVYHF73l68AkWHmitLmI/view?usp=sharing)
* [Commit Virtual 2020 Pubsec Playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZWB92q2KaDvMqRWUsWnz3P)
* [Commit Virtual 2020 Compliance Playlist](https://www.youtube.com/watch?v=aQh2XTHTE0o&list=PLFGfElNsQthafOhkq9M_wYKh1K4dmjg7w)
* [GitLab for PubSec & Regulated Industries Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpEQDAhM3Txia7QHdYlngMa)
* [GitLab for the IC Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpxOMt0YK-ZDnfWxdfKtotv)
* [Digital Transformation & IT Modernization for Gov't & Regulated Industries Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrqCgQwKdxEom3bULFdUi4x)
* [Accelerate Digital Transformation (datasheet)](https://drive.google.com/file/d/1WZTjVTNlkCx-lX9vvVqJlri8Z1euHR8H/view?usp=sharing)
* [ATARC Federal IT Newscast (podcast)](https://open.spotify.com/show/6AU2VxO4j7dtDls4ouxdIq?si=5IB4tzdLTZmt03TtNMBt2g)
* [Building a Modern DevSecOps Software Factory](https://learn.gitlab.com/gov-digital-transformation-6/building-modern-devsecops-software-factory)
* [Growing IT & Business Resilience through DevSecOps](https://learn.gitlab.com/gov-digital-transformation-4/growing-it-business-resilience)
* [Efficiency Messaging for PubSec](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/4558)
* [VPAT Template](https://docs.google.com/document/d/1o6gCe2Se_Cuia-VsrUTdGvESNZVefyoqFAu1NY8rJpQ/) 

* [Strategic Marketing Asset Inventory](https://about.gitlab.com/handbook/marketing/strategic-marketing/asset_inventory/)

***Coming Soon:***

* Application Security for the DevOps Lifecycle: The critical value of shifting security left (S&L focus)
* GitLab Security for the DoD & IC (DoD & IC focus)
* How to Build-In Collaboration Across Your Enterprise
* Empower Your Workforce: Automate Your DevOps & Shift to Higher Value Work
* Ways to Make Your DevOps Procurements More Agile
* Moving to the Cloud with Less Pain (S&L focus)

### Resources 

* [Most Commonly Used Sales Resources](/handbook/marketing/strategic-marketing/sales-resources/)
* [Learn@GitLab Use Case-driven Demo Videos](https://www.youtube.com/playlist?list=PLFGfElNsQthYDx0A_FaNNfUm9NHsK6zED)
* [Competitive Intelligence Resources](/devops-tools/)
* [Global PubSec Marketing Account Profile Template (outside of US)](https://docs.google.com/document/d/1SUS0fbtVpvN4y44AT-BAiPAD32Bbq-hW-CSZco7uiUQ/edit) (use this to compile account-specific knowledge to share with Marketing) 
* [Product Marketing Team Talk Inventory](/handbook/marketing/strategic-marketing/pmmteam/#pmm-talk-inventory)
* [Global PubSec Speakers Bureau (outside of US)](https://drive.google.com/drive/folders/1u47kBHDh9cqOuA683k0MxzwxrZmqobzR?usp=sharing)

### Getting PMM Support    
* [Strategic Marketing Request](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=A-SM-Support-Request)
* [Speaker Request](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new#)

