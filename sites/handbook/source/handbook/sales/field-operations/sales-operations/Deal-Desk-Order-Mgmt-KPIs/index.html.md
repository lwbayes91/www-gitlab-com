---
layout: handbook-page-toc
title: "Deal Desk & Order Management KPI - Results"
description: "This page highlights quarterly KPI results for the Global Deal Desk & Order Management team."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Executive Summary 

This section highlights the current health status of each KPI based on the results of the most recently completed fiscal quarter.

**Definitions:**
- Green: KPI target met or exceeded
- Yellow: KPI target not exceeded; KPI target missed by 10% or less
- Red: KPI target not exceeded; KPI target missed by more than 10%   

| **KPI**                                                                                                                                                                                 | **Current Health Status** |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|
| [Deal Desk Case SLA](/handbook/sales/field-operations/sales-operations/deal-desk/#1-deal-desk--case-slas)                                                                               |      Green      |
| [Deal Desk Support Satisfaction](/handbook/sales/field-operations/sales-operations/deal-desk/#2-regional-support-satisfaction)                                                          |      Green      |
| [Opportunity Approval SLA](/handbook/sales/field-operations/sales-operations/order-management/#1-standard-opportunity-approval-sla)                                                     |      Green      |
| [Opportunity Approval Accuracy](/handbook/sales/field-operations/sales-operations/order-management/#3-opportunity-approval-accuracy-and-efficiency)                                     |      Green      |
| [Regional Support Satisfaction](/handbook/sales/field-operations/sales-operations/order-management/#2-regional-support-satisfaction)                                                    |      Yellow       |


### Key Performance Indicators 

Deal Desk KPIs are outlined on the [Deal Desk handbook page](/handbook/sales/field-operations/sales-operations/deal-desk/#key-performance-indicators). 
Order Management KPIs are outlined on the [Order Management handbook page](/handbook/sales/field-operations/sales-operations/order-management/). 

### FY23 Results 

#### Q1FY23 Results

| **Deal Desk KPIs**                       | **Target** | **Actual (AVG)** | **Total Record Count** | 
|------------------------------------------|------------|------------------|------------------|
| **Deal Desk Case Resolution**            | 6 Hours    | 4 Hours 15 Min   | 9339 Cases |
| Time to close Sales support cases         |            |                  |             |
| **Regional Support Satisfaction**        | 92%        | 92.10%           |     35/38 positive responses        |
| Measured via [Q1FY23 CSAT Survey](https://gitlab.com/gitlab-com/sales-team/field-operations/deal-desk/-/issues/220)               |            |                  |
| **Q2C Efficiency Improvements**          | See: [Q1FY23 CSAT Survey Results](https://gitlab.com/gitlab-com/sales-team/field-operations/deal-desk/-/issues/220)          |  [FY23 Q2C Systems Issues](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/90)        | 44 Q2C Systems Issues opened in Q1FY23 |
| Issues / Documentation to improve Q2C    |            |                  |  |


| **Order Management KPIs**                                        | **Target**   | **Actual (AVG)** | **Total Record Count** |
|------------------------------------------------------------------|--------------|------------------|--------------|
| **Opportunity Approval SLA**                                     |   24 Hours   |  21 Hours 30 Min | 3139 Opportunities |
| Opportunity Booking Process                                      |              |                  |  |
| **Opportunity Approval Accuracy**                                | 95% Accuracy |      95.64%      | 137 rejections | 
| 95% of all opportunities submitted to finance should be accurate |              |                  |  |
| **Regional Support Satisfaction**                                |      92%     |      91.89%      | 34/37 positive responses |
| Measured via [Q1FY23 CSAT Survey](https://gitlab.com/gitlab-com/sales-team/field-operations/deal-desk/-/issues/220)                           |              |                  |  |

*All results are global. For region-specific results, review the [Q1FY23 Global Deal Desk Order Management KPI Results Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/deal-desk/-/issues/229).

#### Q2FY23

Results posted Aug 2023. 

#### Q3FY23

Results Posted Nov 2023. 

#### Q4FY23

Results posted Feb 2024. 
