---
layout: handbook-page-toc
title: Support Time Off Buddies Trial
description: Guidelines for the Support Time Off Buddies trial
---

## Overview

The Support Time Off Buddies program is intended to solve a few problems:

1. Reduce burden of taking time off
1. Provide continuity for customers and reports while out of office
1. Reduce strain on SGGs when out of office
1. Help keep each other accountable for taking time off
1. Help keep each other accountable for not checking in with work during time off

It is currently in a trial phase until October 15, 2022. You can find the
initial proposal and add your thoughts to the discussion on
[the issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4481).

## Current Buddies

| Aric Buerer        | Izzy Fee       |
| Bo Carbonell       | Brie Carranza  |
| Keelan Lang        | Julie Martin   |
| Sam Bernal-Damasco | Eric Rosenberg |

## Responsibilities

Buddies will be expected to adhere to a minimum set of responsibilities. These
responsibilities include:

1. Something
1. Something else
1. Something more

## Tips

While it's up to each buddy to manage their responsibilities, some ideas are
included here to get you started.

### Use pairings

When preparing to leave for time off, or when returning from time off, consider
using a [pairing session](https://gitlab.com/gitlab-com/support/support-pairing/)
to keep your buddy informed.

### Use internal ticket comments

Put a brief comment explaining the state of a ticket and expected interactions
before leaving for time off. This also helps in the event that your buddy needs
to hand off your ticket to someone else in your absence.

### Use Zendesk views

You can set up a Zendesk view to track your buddy's tickets while they're out.

## FAQ

### Am I in charge of my buddy's tickets while they're out?

### Does the Time Off Buddies program account for holidays?

### Does my buddy need to be in the same region as me?

### How many people can I be a buddy for?
