---
layout: markdown_page
title: "Category Direction - Portfolio Management"
description: GitLab supports enterprise Agile portfolio and project management frameworks, including Scaled Agile Framework (SAFe), Scrum, and Kanban. Learn more!
canonical_path: "/direction/plan/portfolio_management/"
---

- TOC
{:toc}

## Portfolio Management

|                       |                               |
| -                     | -                             |
| Stage                 | [Plan](/direction/plan/)      |
| Maturity              | [TBD](/direction/maturity/) |
| Content Last Reviewed | `2022-04-25`                  |


## Overview

### Purpose

GitLab's vision is to provide Portfolio Management tools for DevOps that help our customers determine which opportunities have higher ROI when making strategic business planning decisions. 

Enterprises work on complex initiatives that cut across multiple teams and departments, often spanning months, quarters, and even years. We support organizing initiatives into powerful multi-level work breakdown plans and to enable tracking the execution of them over time. We want to enable organizations to track efforts in flight and plan upcoming work to best utilize their resources and focus on the right priorities. 

GitLab supports popular [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/), including Scaled Agile Framework [(SAFe)](https://about.gitlab.com/solutions/agile-delivery/scaled-agile/), [Scrum, and Kanban](https://about.gitlab.com/solutions/agile-delivery/).


|          |          |
| ---      | ---      |
| ![epicstree-direction.png](/direction/plan/portfolio_management/epicstree-direction.png)  | ![roadmaps-direction.png](/direction/plan/portfolio_management/roadmaps-direction.png)   |


### What's next & why

To move towards our long term vision of robust portfolio management features, we are focusing on providing a foundation of functionality that we can build on in the future. 

The main goal at this time are: 

1. [Converting epics to work items](https://gitlab.com/groups/gitlab-org/-/epics/6033)
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587) 
1. [Saved Queries and Views](https://gitlab.com/groups/gitlab-org/-/epics/5516)

Future items iclude:

- High-level [strategic initiatives and OKRs (objectives and key results)](https://gitlab.com/gitlab-org/gitlab/issues/36775)
- Consolidating and improving configuration of Plan capabilities. This will set the groundwork for out of the box support for to SAFe.
- Other improvements increase the maturity of [Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/2649) 

### What we're not doing

- In the next two years, we don't plan on integrating with large scale accounting systems 


## Maturity plan

Now that we've combined Roadmaps and Epics into this category, we need to **reset our category maturity** for Portfolio Management.

Previous maturities for reference: 

-  **Epics** are now a ~"type::feature" but was at the **viable** level, and our next maturity target was **complete** by 2021-10-30. Progress: [Viable](https://gitlab.com/groups/gitlab-org/-/epics/967) and [loveable](https://gitlab.com/groups/gitlab-org/-/epics/968)
-  **Roadmaps** are now a ~"type::feature" but was at the **minimal** level, and our next maturity target was **viable** by 2021-09-28 see [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80922). We are tracking progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1998). Progress: [Complete](https://gitlab.com/groups/gitlab-org/-/epics/2002)


## Top customer success/sales issue(s)


To support our Customer Success and Sales departments, we are validating and working towards critical items to enable them to serve additional prospects and customers:

1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864) [DONE]
1. [Provide Project Level Epics](https://gitlab.com/gitlab-org/gitlab/-/issues/31840) 
1. [Enable mapping Epics as "Related To" other Epics](https://gitlab.com/gitlab-org/gitlab/-/issues/202431)[DONE]
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587) 
1. [Introduce colors for epics (and roadmap view in particular)](https://gitlab.com/gitlab-org/gitlab/-/issues/7641)


## Competitive landscape

### Epics

Leveraging Epics as a building block for Product and Portfolio Management is a common use case in the industry, with many are established players such as Jira, Clarity, Planview, VersionOne, AgileCraft, and ServiceNow offering similar functionality. Many of these tools were developed targeted at truly enterprise cases, allowing users to track large business initiatives across an organization. Customers using these tools typically have another set of tools for the product-development teams to turn these high-level business initiatives into scoped out detailed planned work and actual software deliverables. Therefore, our competitive advantage is having _both_ (the high-level initiatives, and the product-development-level abstractions) in a single tool, that is fully integrated for a seamless experience. Our strategy is building _toward_ those enterprise use cases, starting with the product-development baseline abstractions. 



### Roadmaps

The Roadmapping space is large and is made up of stand alone tools, and as additional functionality on top of software planning systems like AHA.io, Roadmunk, Productboard, Monday.com, Trello, and Productplan. Many of these  were developed targeted at truly enterprise cases, allowing users to track large business initiatives across an organization. Customers using these tools often have another set of tools for the product-development teams to turn these high-level business initiatives into scoped out detailed planned work and actual software deliverables. Therefore, our competitive advantage is having _both_ (the high-level initiatives, and the product-development-level abstractions) in a single tool, that is fully integrated for a seamless experience. In addition to the enterprise opportunity, we believe roadmapping can be also be beneficial to smaller organizations and individual teams to chart out the future and report on progress. Our strategy is building _toward_  enterprise use cases starting with the product-development baseline abstractions. 
