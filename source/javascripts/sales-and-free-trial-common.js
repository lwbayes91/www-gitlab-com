/* eslint-disable */

(function() {
  var paramsFetcher = function() {
    // Looks for each of the params in the url, e.g.?key=value
    var regex = /[?&]([^=#]+)=([^&#]*)/g;
    var params = {};
    var match = '';

    // eslint-disable-next-line no-cond-assign
    while (match = regex.exec(window.location.href)) {
      params[match[1]] = match[2];
    }

    return (Object.keys(params).length > 0) ? params : null;
  };

  var paramsParser = function(params) {
    if (!params) return null;
    var validParams = {
      email: {}
    };

    Object.keys(params).forEach(function(param) {
      if (param.toLowerCase() === 'email') {
        validParams.email = params[param];
      }
    });

    return validParams;
  };

  MktoForms2.setOptions(
  {
    formXDPath : "/rs/194-VVC-221/images/marketo-xdframe-relative.html"
  });
  MktoForms2.loadForm('//page.gitlab.com', '194-VVC-221', 1318, function(form) {
    form.onSuccess(function(values, followUpUrl) {
      dataLayer.push(
      {
        'event' : 'freeTrialandSales', 
        'mktoFormId' : form.getId(),
        'eventCallback' : function()
        {
          // hide the form after submission
          form.getFormElem().hide();
          // check for cookiebot message. if one exists, hide it.
          var targets=document.querySelectorAll('.cbnc-message');
          for(loopcount=0;loopcount<targets.length;loopcount++)
          {
            targets[loopcount].hide();
          };
          // show the confirmation
          document.getElementById('confirmform').style.visibility = 'visible';
          // scroll back to the top so the confirmation form can be seen
          $('html, body').animate({scrollTop: parseInt($('.confirmform').offset().top-100)}, 500);
        }, 'eventTimeout' : 3000
      });
      return false;
    });

    var urlParams = paramsFetcher();
    var parsedParams = paramsParser(urlParams);

    if (parsedParams) {
      $('input#Email').val(parsedParams.email);
    }
  });
})();
